<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	public function index()
	{
		$data['barang'] = $this->db->get('barang')->result_array();

		$this->load->view('barang', $data);
	}

	public function tambahbarang()
	{
		$this->form_validation->set_rules("nama", "nama", "required");
		$this->form_validation->set_rules("harga", "harga", "required");
		$this->form_validation->set_rules("deskripsi", "deskripsi", "required");


		if ($this->form_validation->run() == False) {
			$this->load->view('tambahbarang');
		} else {
			$data = [
				"nama" => $this->input->post("nama"),
				"harga" => $this->input->post("harga"),
				"deskripsi" => $this->input->post("deskripsi")
			];

			$this->db->insert('barang', $data);
			redirect(base_url());
		}
	}
}
