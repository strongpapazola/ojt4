<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>

    <title>Payment Hipmikimdo</title>
  </head>
  <body style="background-color: #f5f5f5;">
    
  
<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="container">
        <img src="https://hipmikimdo-id.org/wp-content/uploads/2021/04/favicon-3.png" style="width: 50px;">
        <a class="navbar-brand ml-3" href="#" style="color: #018117;"><strong>Hipmikimdo</strong></a>
        <!-- <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button> -->
        <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
            <div class="navbar-nav">
                <div class="nav-item nav-link h3" style="color: #018117;">|</div>
                <a class="navbar-brand nav-item nav-link mt-2" href="#" style="color: #018117;">Payment</a>
            </div>
        </div>
    </div>
</nav>

<div class="container">
    <div class="row mt-3">
        <div class="col-12">

            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">
                        <i class="fas fa-receipt mr-2"></i>
                        Pembayaran Hipmikimdo
                    </h5>
                    <hr>

                    <div class="row">
                        <div class="col-md-6 mt-3 text-center">
                            <div class="card-header setMiddleBox" style="min-height: 150px">
                                <div class="row" id="middleBalance" style="height: 22px;"></div>
                                <i id="default-img" class="fas fa-money-bill-wave fa-5x text-secondary"></i>
                                <img class="align-middle" id="dana-img" src="https://upload.wikimedia.org/wikipedia/commons/thumb/7/72/Logo_dana_blue.svg/1200px-Logo_dana_blue.svg.png" style="max-width: 200px; display: none;">
                                <img class="align-middle" id="bayarind-img" src="https://tesla.malline.id/linedata/uploads/Logo-BayarInd-600x196.png" style="max-width: 200px; display: none;">
                                <img class="align-middle" id="bca-img" src="https://cdn.worldvectorlogo.com/logos/bca-bank-central-asia.svg" style="max-width: 200px; display: none;"> 
                                <img class="align-middle" id="bni-img" src="https://upload.wikimedia.org/wikipedia/id/thumb/5/55/BNI_logo.svg/1200px-BNI_logo.svg.png" style="max-width: 200px; display: none;">

                            </div>
                        </div>
                        <div class="col-md-6 mt-3">
                            <form action="" method="post">
                            <!-- <div class="form-group">
                                <label for="exampleInputEmail1">Email address</label>
                                <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
                                <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Password</label>
                                <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
                            </div>
                            <div class="form-check">
                                <input type="checkbox" class="form-check-input" id="exampleCheck1">
                                <label class="form-check-label" for="exampleCheck1">Check me out</label>
                            </div> -->
                            <div class="custom-control custom-radio">
                                <input type="radio" id="dana" name="customRadio" class="custom-control-input">
                                <label class="custom-control-label" for="dana">DANA</label>
                            </div>
                            <div class="custom-control custom-radio">
                                <input type="radio" id="bayarind" name="customRadio" class="custom-control-input">
                                <label class="custom-control-label" for="bayarind">Bayarind</label>
                            </div>
                            <div class="custom-control custom-radio">
                                <input type="radio" id="bca" name="customRadio" class="custom-control-input">
                                <label class="custom-control-label" for="bca">Virtual Account BCA</label>
                            </div>
                            <div class="custom-control custom-radio">
                                <input type="radio" id="bni" name="customRadio" class="custom-control-input">
                                <label class="custom-control-label" for="bni">Virtual Account BNI</label>
                            </div>
                            <!-- <button type="submit" class="btn btn-primary">Submit</button> -->
                        </form>
                        
                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>
</div>


<script>
$(function(){

    function setMiddle(idimg) {
        var heightBox = $(".setMiddleBox").height();
        console.log("Panjang Box : "+heightBox);
        var heightItem = $(idimg).height();
        console.log("Panjang Item : "+heightItem);
        
        var middleBalance = (heightBox - heightItem) / 2;
        console.log("Panjang Item : "+middleBalance);
        $("#middleBalance").css("height", middleBalance);
    }

    $("#dana").on("click", function(){
        setMiddle("#dana-img");
        $("#default-img").css("display", "none");
        $("#dana-img").css("display", "");
        $("#bayarind-img").css("display", "none");
        $("#bca-img").css("display", "none");
        $("#bni-img").css("display", "none");
    })
    $("#bayarind").on("click", function(){
        setMiddle("#bayarind-img");
        $("#default-img").css("display", "none");
        $("#dana-img").css("display", "none");
        $("#bayarind-img").css("display", "");
        $("#bca-img").css("display", "none");
        $("#bni-img").css("display", "none");
    })
    $("#bca").on("click", function(){
        setMiddle("#bca-img");
        $("#default-img").css("display", "none");
        $("#dana-img").css("display", "none");
        $("#bayarind-img").css("display", "none");
        $("#bca-img").css("display", "");
        $("#bni-img").css("display", "none");
    })
    $("#bni").on("click", function(){
        setMiddle("#bni-img");
        $("#default-img").css("display", "none");
        $("#dana-img").css("display", "none");
        $("#bayarind-img").css("display", "none");
        $("#bca-img").css("display", "none");
        $("#bni-img").css("display", "");
    })
    
})
</script>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  </body>
</html>